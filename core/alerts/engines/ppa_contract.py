# import pandas as pd
# from datetime import datetime, date
# from dateutil.relativedelta import relativedelta
# from django.utils.translation import gettext as _
# from core.analysis.pv.exceptions import NoData
# from core.analysis.pv.models import get_generation_summary, get_ppa_contracts, get_generation_data, \
#     get_energy_totals, get_historic_data, get_consumption_by_hours, get_feeding_data
# from core.models import Language
# from core.alerts.models import Alert, AlertType
# from core.alerts.engines.base import BaseAlertEngine
# from core.alerts.utils import get_alert_type
# import locale


def get_last_dates(days=1, **kwargs):
    now = kwargs.pop('now', datetime.now())
    end = datetime(now.year, now.month, now.day)
    start = end - relativedelta(days=days)
    return start, end


def get_pv_alerts(building):
    alert_type_ids = AlertType.objects(codename__in=PV_ALERT_TYPES.keys()).values_list("id")
    alerts = Alert.objects.filter(building=building, type_id__in=alert_type_ids)
    return alerts


PV_ALERT_TYPES = {
    AlertType.YEARLY_GENERATION_ALERT: _("Incumplimiento del compromiso de producción anual"),
    AlertType.SUSPENSIVE_CONDITION_ALERT: _("Retraso en la entrada en operación de la planta fotovoltaica"),
    AlertType.LOW_CONSUMPTION_ALERT: _("Baja producción de la planta fotovoltaica por una reducción del consumo"),
    AlertType.LOW_PRODUCTION_ALERT: _("Baja producción de la planta fotovoltaica"),
    AlertType.ZERO_PRODUCTION_ALERT: _('Producción nula de energía fotovoltaica'),
    AlertType.ZERO_CONSUMPTION_ALERT: _('Consumo nulo en el edificio'),
    AlertType.PPA_CREATED_BC: _('PPA creado en la blockchain'),
    AlertType.PPA_SIGNED_BC: _('PPA firmado en la blockchain'),
}


class PPAAlertEngine(BaseAlertEngine):

    alert_type = None
    default_level = Alert.HIGH

    def evaluate(self, building, *args, **kwargs):
        alerts = []
        ppa_lst = args or get_ppa_contracts(building)
        for ppa in ppa_lst:
            alert = self._evaluate_contract(ppa, **kwargs)
            if alert:
                alerts.append(alert)
        return alerts

    def _evaluate_contract(self, ppa, **kwargs):
        return None

    def _create_alert(self, ppa, **kwargs):
        alert = Alert()
        alert.language = Language.DEFAULT_LANGUAGE_NAME
        alert.level = kwargs.get('level') or self.default_level
        alert.type_id = get_alert_type(self.alert_type)
        alert.date_created = kwargs.get('now') or datetime.now()
        alert.building = ppa.building
        alert.src_meter = ppa.pv_meter
        alert.unread = True
        alert.subject = self.SUBJECT_MSG
        locale.setlocale(locale.LC_ALL, 'es-ES')            # TODO 1 format should occur in the moment of printing
        alert.description = self.DESCRIPTION % kwargs
        alert.translate(**{'subject': self.SUBJECT_MSG, 'description': self.DESCRIPTION, 'description_params': kwargs})
        return alert


class YearlyGenerationAlert(PPAAlertEngine):

    alert_type = AlertType.YEARLY_GENERATION_ALERT

    SUBJECT_MSG = _('Incumplimiento del compromiso de producción anual')
    # DESCRIPTION = _("La producción total de {production:n} kWh el año {year} no alcanzó el compromiso establecido "
    #                 "en el contrato PPA que es de {commitment:n} kWh.")
    DESCRIPTION = _("La producción total de %(production)s kWh el año %(year)s no alcanzó el compromiso establecido "
                    "en el contrato PPA que es de %(commitment)s kWh.")

    def _evaluate_contract(self, ppa, **kwargs):
        today = kwargs.pop('now', datetime.now())
        year = today.year - 1
        pv_meter = ppa.pv_meter
        summary = get_generation_summary(pv_meter, today)
        from_date = ppa.from_date
        if from_date.year == today.year or from_date.month != today.month or from_date.day != today.day:
            return None
        df = pd.DataFrame(summary)
        if not df.empty:
            df = df.set_index("_id", drop=True)
            try:
                value = df.loc[year]["yearlySum"]
                if value < ppa.min_yearly_generation:
                    kwargs['year'] = year
                    kwargs['production'] = value
                    kwargs['commitment'] = ppa.min_yearly_generation
                    return self._create_alert(ppa, **kwargs)
            except KeyError:
                raise NoData
        return None


class SuspensiveConditionAlert(PPAAlertEngine):

    alert_type = AlertType.SUSPENSIVE_CONDITION_ALERT

    SUBJECT_MSG = _('Retraso en la entrada en operación de la planta fotovoltaica')
    # DESCRIPTION = _("La operación de la planta fotovoltaica no ha entrado en funcionamiento en la fecha {start_date!s} "
    #                 "prevista en el contrato PPA")
    DESCRIPTION = _(
        "La operación de la planta fotovoltaica no ha entrado en funcionamiento en la fecha %(start_date)s "
        "prevista en el contrato PPA")

    def _evaluate_contract(self, ppa, **kwargs):
        date_ref = ppa.date_from + relativedelta(months=ppa.suspension_cond_duration)
        now = kwargs.pop('now', datetime.now())
        if date_ref.date() == now.date():
            df = get_generation_data(ppa.pv_meter, ppa.date_from, date_ref)
            if df.empty:
                kwargs['start_date'] = date_ref
                return self._create_alert(ppa, **kwargs)
        return None


class LowConsumptionAlert(PPAAlertEngine):

    alert_type = AlertType.LOW_CONSUMPTION_ALERT

    SUBJECT_MSG = _('Merma de producción de energía fotovoltaica por una reducción del consumo del edificio')
    # DESCRIPTION = _("En el día {day:%a, %d %b %Y} se produjo un vertido a red de más de un {factor:n}% de la "
    #                 "producción fotovoltaica, que puede estar relacionado con un bajo consumo de la instalación.")
    DESCRIPTION = _("En el día %(day)s se produjo un vertido a red de más de un %(factor)s de la "
                    "producción fotovoltaica, que puede estar relacionado con un bajo consumo de la instalación.")

    def _evaluate_contract(self, ppa, **kwargs):
        start, end = get_last_dates(days=1, **kwargs)
        try:
            df = get_feeding_data(ppa.supply_point, start, end)
            df.columns = ['production', 'replacement', 'feeding']
            df = df.sum(axis=0)
            factor = ppa.retailer.parameters.pv_alert_low_consumption_thres
            if df.feeding > df.production * (factor / 100):
                kwargs['factor'] = factor
                kwargs['day'] = start
                return self._create_alert(ppa, **kwargs)
        except NoData:
            pass

        return None


class LowProductionAlert(PPAAlertEngine):

    alert_type = AlertType.LOW_PRODUCTION_ALERT

    SUBJECT_MSG = _('Baja producción de la planta fotovoltaica')
    # DESCRIPTION = _("El valor de la producción en los últimos {days} días es anormalmente bajo en relación al "
    #                 "histórico de datos de producción de la planta. Esto que puede deberse a incidencias en la "
    #                 "instalación que demandan mantenimiento correctivo.")
    DESCRIPTION = _("El valor de la producción en los últimos %(days)s días es anormalmente bajo en relación al "
                    "histórico de datos de producción de la planta. Esto que puede deberse a incidencias en la "
                    "instalación que demandan mantenimiento correctivo.")

    def _evaluate_contract(self, ppa, **kwargs):
        days = 30
        start, end = get_last_dates(days=days, **kwargs)
        if start > ppa.date_from:
            df = get_historic_data(ppa.pv_meter, start, end)
            if not df.empty:
                ref = df.iloc[:, 0:-1].mean(axis=1) if df.columns.size > 1 \
                    else df.iloc[:, -1].mean(axis=0)
                df = df.iloc[:, -1] < ref
                num = df.values.sum()
                factor = ppa.retailer.parameters.pv_alert_low_production_thres / 100
                if num > factor * days:
                    kwargs['days'] = days
                    kwargs['level'] = Alert.HIGH if num > 0.7 * days else Alert.NORMAL
                    return self._create_alert(ppa, **kwargs)
        return None


class ZeroProductionAlert(PPAAlertEngine):

    alert_type = AlertType.ZERO_PRODUCTION_ALERT
    default_level = Alert.HIGH

    SUBJECT_MSG = _('Producción nula de energía fotovoltaica')
    # DESCRIPTION = _("La planta no ha producido energía a lo largo del día {date:%a, %d %b %Y}."
    #                 " Es necesario identificar la causa o problema existente con urgencia.")
    DESCRIPTION = _("La planta no ha producido energía a lo largo del día %(date)s."
                    " Es necesario identificar la causa o problema existente con urgencia.")

    def _evaluate_contract(self, ppa, **kwargs):
        start, end = get_last_dates(days=1, **kwargs)
        df = get_energy_totals(ppa.pv_meter, start, end)
        if df.empty or df['generationTotal'][0] <= 0:
            kwargs['date'] = start
            return self._create_alert(ppa, **kwargs)
        return None


class ZeroConsumptionAlert(PPAAlertEngine):

    alert_type = AlertType.ZERO_CONSUMPTION_ALERT
    default_level = Alert.HIGH

    SUBJECT_MSG = _('Consumo nulo en el edificio')
    # DESCRIPTION = _("A lo largo del día {day:%a, %d %b %Y} se registraron horas con consumo nulo en la instalación. "
    #                 "En estos casos la energía producida en la planta fotovoltaica no se ha autoconsumido, "
    #                 "vertiéndose a red.")
    DESCRIPTION = _("A lo largo del día %(day)s se registraron horas con consumo nulo en la instalación. "
                    "En estos casos la energía producida en la planta fotovoltaica no se ha autoconsumido, "
                    "vertiéndose a red.")

    def _evaluate_contract(self, ppa, **kwargs):
        start, end = get_last_dates(days=1, **kwargs)
        df = get_consumption_by_hours(ppa.pv_meter, start, end)
        has_zero_consumption = False
        if not df.empty:
            df_zero_hourly_consumption = df[df['hourly_consumption'] == 0]
            has_zero_consumption = True if not df_zero_hourly_consumption.empty else False
        if df.empty or has_zero_consumption:
            kwargs['day'] = start
            return self._create_alert(ppa, **kwargs)
        return None


class PPACreatedBCAlert(PPAAlertEngine):

    alert_type = AlertType.PPA_CREATED_BC

    SUBJECT_MSG = _('Contrato registrado en la Blockchain')
    DESCRIPTION = _("El contrato ha sido creado de forma satisfactoria en la blockchain.")

    def _evaluate_contract(self, ppa, **kwargs):
        if ppa.bc_contract_info and ppa.bc_signed is False:
            return self._create_alert(ppa, **kwargs)
        return None


class PPASignedBCAlert(PPAAlertEngine):
    alert_type = AlertType.PPA_SIGNED_BC

    SUBJECT_MSG = _('Contrato firmado en la Blockchain')
    DESCRIPTION = _("El contrato ha sido firmado de forma satisfactoria en la blockchain.")

    def _evaluate_contract(self, ppa, **kwargs):
        if ppa.bc_contract_info and ppa.bc_signed is True:
            return self._create_alert(ppa, **kwargs)
        return None

class PPAModifideContractBCAlert(PPAAlertEngine):

    alert_type = AlertType.PPA_MODIFIED_CONTRACT_BC

    SUBJECT_MSG = _('Contrato modificado')
    DESCRIPTION = _("Los datos no coinciden con los de blockchain.")

    def _evaluate_contract(self, ppa, **kwargs):
        if ppa.bc_contract_info and ppa.bc_signed:
            return self._create_alert(ppa, **kwargs)
        return None


class AlertType(TranslateDocumentMixin, Document):

    PPA_CREATED_BC = "PPACreatedBCAlert"
    PPA_SIGNED_BC = "PPASignedBCAlert"
    PPA_MODIFIED_CONTRACT_BC = "PPAModifiedContractBCAlert"
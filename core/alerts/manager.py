from core.alerts.models import AlertType, Alert

from core.alerts.engines.ppa_contract import PPACreatedBCAlert, PPASignedBCAlert, PPAModifideContractBCAlert


class AlertManager(object):
    ALERT_ENGINES = {
        AlertType.PPA_CREATED_BC: PPACreatedBCAlert(),
        AlertType.PPA_SIGNED_BC: PPASignedBCAlert(),
        AlertType.PPA_MODIFIED_CONTRACT_BC: PPAModifideContractBCAlert(),
    }


class PPAContract(ContractBase):

    CONTRACT_TYPE = (
        (13, _('PPA - Power Purchase Agreement')),
    )

    PPA_TYPE = (
        (0, _('Fotovoltaico')),
    )

    ppa_type = IntField(choices=PPA_TYPE, required=True, default=0, null=False)
    investor = ReferenceField(UserProfile, required=True)

    building = ReferenceField(Building, required=True)

    contract_duration = IntField(required=True, min_value=0, default=5)
    suspension_cond_duration = IntField(required=True, min_value=0, default=5)
    settlement_frequency = IntField(required=True, min_value=0, default=5)

    installed_power = FloatField(required=True, min_value=0, default=1)
    min_yearly_generation = FloatField(required=True, min_value=0, default=0)

    energy_price = FloatField(required=True, min_value=0, default=0)
    min_energy_purchase_ratio = FloatField(required=True, min_value=0, max_value=100, default=0)
    over_min_price = FloatField(required=True, min_value=0, default=0-0)
    over_min_energy_ratio = FloatField(required=True, min_value=0, max_value=100, default=0.0)
    excess_energy_price = FloatField(required=True, min_value=0, max_value=100, default=0.0)

    # porcentage de energía generada por debajo del mínimo
    under_min_price = FloatField(required=True, min_value=0, default=0.0)
    under_min_ratio = FloatField(required=True, min_value=0, max_value=100, default=0.0)

    monetary_account_of_guarantee = FloatField(required=False, min_value=0, default=0.0)

    total_investment = FloatField(required=False, min_value=0, default=0.0)
    yearly_maintenance_cost = FloatField(required=False, min_value=0, default=0.0)

    bc_contract_info = DictField(required=False, default={})
    bc_signed = BooleanField(required=False, default=None)
    bc_last_registered_consumption_date = DateTimeField(required=False)

    meta = {'strict': False, 'allow_inheritance': True}

    def save(self, *args, **kwargs):
        if self.bc_contract_info and self.bc_signed is None:
            self.bc_signed = False
        super(PPAContract, self).save(*args, **kwargs)

    @property
    def should_register_consumption(self) -> bool:
        if self.bc_last_registered_consumption_date is None:
            return True
        current_date = datetime.now()
        return self.bc_last_registered_consumption_date.month != current_date.month \
               or self.bc_last_registered_consumption_date.year != current_date.year

    @property
    def pv_meter(self):
        return self.supply_point        # TODO 1 prefent this by changing the contract property name to 'meter'

    @property
    def current_progress(self):                # TODO 1 promote to parent class
        def diff_month(d1, d2):
            return (d1.year - d2.year) * 12 + d1.month - d2.month
        today = datetime.now()
        if today <= self.date_from:
            return 0
        elapsed = diff_month(today, self.date_from)
        duration = self.contract_duration * 12
        if elapsed > duration:
            return 100
        return round((elapsed / duration) * 100, 0)

    @property
    def is_active(self):
        return 0 <= self.current_progress < 100

    def clean(self):
        # TODO 1 custom validations
        return super(PPAContract, self).clean()

    def get_contract_type(self):
        contract_type = _("PPA")
        return str(contract_type)

    def get_ppa_type(self):
        ppa_type = _('Fotovoltaico')
        return str(ppa_type)

    def get_energy_prices(self, date_from, date_to):
        pass

    def to_string(self):            # TODO 1 move to contractbase
        contract_type = self.get_contract_type()
        return '%s' % contract_type

    def to_string_with_dates(self):
        contract_type = self.get_contract_type()
        ppa_type = self.get_ppa_type()
        return f'{contract_type} {ppa_type} ({self.date_from.strftime("%d/%m/%Y")} - {self.date_to.strftime("%d/%m/%Y")})'

    def __str__(self):
        return self.to_string_with_dates()

    def __unicode__(self):
        return self.to_string()
bc_last_registered_consumption_date.year != current_date.year
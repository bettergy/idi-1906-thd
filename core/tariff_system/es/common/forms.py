

class PPAContractForm(ContractForm):
    supply_point = d_fields.HiddenInput()
    retailer = d_fields.HiddenInput()
    building = d_fields.HiddenInput()  # TODO 1 promote to parent
    # currency_code = d_fields.HiddenInput()
    date_to = d_fields.HiddenInput()

    ppa_type_choices = [(f[0], str(f[1])) for f in PPAContract.PPA_TYPE]
    ppa_type = m_fields.ListField(choices=ppa_type_choices, required=True)

    investor = m_fields.ListField(required=True)

    contract_duration = d_fields.FloatField(label=_('Duración del contrato'),
                                            widget=InputGroupWidget(base_widget=d_fields.NumberInput, data=_('años'),
                                                                    attrs={'class': 'form-control parameter'}),
                                            required=True, initial=0, min_value=0)

    suspension_cond_duration = d_fields.FloatField(label=_('Duración de condición suspensiva'),
                                                   widget=InputGroupWidget(base_widget=d_fields.NumberInput, data=_('meses'),
                                                                           attrs={'class': 'form-control parameter'}),
                                                   required=True, initial=0, min_value=0)

    settlement_frequency = d_fields.FloatField(label=_('Frecuencia de liquidación'),
                                               widget=InputGroupWidget(base_widget=d_fields.NumberInput, data=_('meses'),
                                                                       attrs={'class': 'form-control parameter'}),
                                               required=True, initial=0, min_value=0)

    installed_power = d_fields.FloatField(label=_('Potencia instalada'),
                                          widget=LocalizedNumberInput(data='kWp',
                                                                      attrs={'class': 'form-control parameter numbersOnly'}),
                                          required=True, initial=1.0, localize=True, min_value=0)

    min_yearly_generation = d_fields.FloatField(label=_('Generación mínima de energía anual'),
                                                widget=LocalizedNumberInput(data='kWh',
                                                                            attrs={'class': 'form-control parameter numbersOnly'}),
                                                required=True, initial=0.0, localize=True, min_value=0)

    energy_price = d_fields.FloatField(label=_('Precio de energía'),
                                       widget=LocalizedNumberInput(data='€/MWh',
                                                                   attrs={'class': 'form-control parameter numbersOnly',
                                                                          'min': "0", 'value': "0", 'step': ".01"}),
                                       required=True, initial=0.0, localize=True, min_value=0)

    min_energy_purchase_ratio = d_fields.FloatField(label=_('Porcentaje obligatorio de compra de energía minima'),
                                                    widget=LocalizedNumberInput(data='%',
                                                                                attrs={'class': 'form-control parameter numbersOnly'}),
                                                    required=True, initial=0.0, localize=True, min_value=0)

    over_min_price = d_fields.FloatField(label=_('Precio de energía generada sobre el mínimo'),
                                         widget=LocalizedNumberInput(data='€/MWh',
                                                                     attrs={'class': 'form-control parameter numbersOnly'}),
                                         required=True, initial=0.0, localize=True, min_value=0)

    over_min_energy_ratio = d_fields.FloatField(label=_('Porcentaje de energía generada sobre el mínimo'),
                                                widget=LocalizedNumberInput(data='%',
                                                                            attrs={'class': 'form-control parameter numbersOnly'}),
                                                required=True, initial=0.0, localize=True, min_value=0)

    excess_energy_price = d_fields.FloatField(label=_('Precio de energía de exceso generada sobre el mínimo'),
                                              widget=LocalizedNumberInput(data='€/MWh',
                                                                          attrs={'class': 'form-control parameter numbersOnly'}),
                                              required=True, initial=0.0, localize=True, min_value=0)

    under_min_price = d_fields.FloatField(label=_('Precio de energía generada por debajo del mínimo'),
                                          widget=LocalizedNumberInput(data='€/MWh',
                                                                      attrs={'class': 'form-control parameter numbersOnly'}),
                                          required=True, initial=0.0, localize=True, min_value=0)

    under_min_ratio = d_fields.FloatField(label=_('Porcentaje de energía generada por debajo del mínimo numbersOnly'),
                                          widget=LocalizedNumberInput(data='%',
                                                                      attrs={'class': 'form-control parameter numbersOnly'}),
                                          required=True, initial=0.0, localize=True, min_value=0)

    monetary_account_of_guarantee = d_fields.FloatField(label=_('Cantidad monetaria de la garantía'),
                                                        widget=LocalizedNumberInput(data='€',
                                                                                    attrs={'class': 'form-control parameter numbersOnly'}),
                                                        required=True, initial=0.0, localize=True, min_value=0)

    total_investment = d_fields.FloatField(label=_('Inversión de la planta fotovoltaica'),
                                           widget=LocalizedNumberInput(data='€',
                                                                       attrs={'class': 'form-control parameter numbersOnly'}),
                                           required=False, initial=0.0, localize=True, min_value=0)

    yearly_maintenance_cost = d_fields.FloatField(label=_('Coste de mantenimiento anual'),
                                                  widget=LocalizedNumberInput(data='€',
                                                                              attrs={'class': 'form-control parameter numbersOnly'}),
                                                  required=False, initial=0.0, localize=True, min_value=0)

    bc_contract_info = d_fields.HiddenInput()
    bc_signed = d_fields.HiddenInput()

    def __init__(self, *args, **kwargs):
        super(PPAContractForm, self).__init__(*args, **kwargs)

        contract_type_choices = [(f[0], str(f[1])) for f in PPAContract.CONTRACT_TYPE]
        self.fields['contract_type'].choices = contract_type_choices
        self.fields['ppa_type'].choices = self.ppa_type_choices

        # TODO 0 utility = Utility.objects.get(id=utility_id)
        qs = UserProfile.objects.filter(role=UserProfile.INVESTOR)  # TODO 0 """utility=utility.id, """
        self.fields["investor"].queryset = qs
        self.fields["investor"].choices = qs.values_list('id', 'revisor_name')

        instance = kwargs.get('instance', None)
        if instance and instance.building:
            self.fields["building"].choices = [(instance.building.id, instance.building.name)]

        bc_contract_info = instance.bc_contract_info if instance else {}
        self.fields["bc_contract_info"].initial = json.dumps(bc_contract_info)

    # def clean_bc_signed(self):
    #     if not self.cleaned_data["bc_signed"]:
    #         return self.cleaned_data["bc_signed"]
    #
    #     if self.cleaned_data["bc_signed"] == "1":
    #         value = True
    #     else:
    #         value = False
    #     return value

    def clean_bc_contract_info(self):
        import json
        if not self.cleaned_data["bc_contract_info"]:
            return self.cleaned_data["bc_contract_info"]

        value = json.loads(self.cleaned_data["bc_contract_info"])

        return value

    def save(self, commit=True):
        instance = super(PPAContractForm, self).save(commit=False)
        # instance.date_to = instance.date_from + relativedelta(years=int(instance.contract_duration))
        if commit:
            instance.save()
            return instance, "success"
        else:
            return instance

    class Meta:
        model = PPAContract
        fields = ['ppa_type', 'retailer', 'building', 'investor', 'contract_type', 'tariff', 'tariff_group',
                  'date_from', 'date_to', 'simulated', 'simulated_name', "supply_point",
                  'contract_duration', 'suspension_cond_duration', 'settlement_frequency',
                  'installed_power', 'min_yearly_generation', 'energy_price',
                  'min_energy_purchase_ratio', 'over_min_price', 'over_min_energy_ratio', 'excess_energy_price',
                  'under_min_price', 'under_min_ratio',
                  'monetary_account_of_guarantee',
                  'total_investment', 'yearly_maintenance_cost',
                  'bc_contract_info', 'bc_signed']

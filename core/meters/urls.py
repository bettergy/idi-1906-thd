from django.conf.urls import url


urlpatterns = [
    url(r'^get_ppa_not_signed_contracts', PPAContractsNotSignedView.as_view(), name='get_ppa_not_signed_contracts'),
    url(r'^create_ppa_alert', BlockchainAlert.as_view(), name='create_ppa_alert'),
]

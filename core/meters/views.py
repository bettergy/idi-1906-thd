

class ContractView(LoginRequiredMixin, TemplateView):

    def get_context_data(self, **kwargs):
        context = super(ContractView, self).get_context_data(**kwargs)
        should_register_data_in_blockchain = False

        # { . . . }

        bc_contract_info = {}
        if isinstance(self.contract, PPAContract):
            bc_contract_info = self.contract.bc_contract_info if self.contract and self.contract.bc_contract_info else {}
            should_register_data_in_blockchain = self.contract.should_register_consumption

        context["should_register_data_in_blockchain"] = should_register_data_in_blockchain
        context["bc_contract_info"] = json.dumps(bc_contract_info)

        # { . . . }

        if self.form.is_valid():
            self.contract, msg = self.form.save()

            if msg is "success":

                from core.alerts.manager import AlertManager
                from core.alerts.models import AlertType
                alert_type_ppa_created = AlertType.objects.get(codename=AlertType.PPA_CREATED_BC)
                if Alert.objects.filter(building=self.contract.building, type_id=alert_type_ppa_created).count() == 0:
                    AlertManager().evaluate_alert(self.contract.building, AlertType.PPA_CREATED_BC, self.contract,
                                                  **kwargs)
                alert_type_ppa_signed = AlertType.objects.get(codename=AlertType.PPA_SIGNED_BC)
                if Alert.objects.filter(building=self.contract.building, type_id=alert_type_ppa_signed).count() == 0:
                    AlertManager().evaluate_alert(self.contract.building, AlertType.PPA_SIGNED_BC, self.contract,
                                                  **kwargs)

class PPAContractsNotSignedView(LoginRequiredMixin, DeleteView):
    model = PPAContract
    template_name = 'meter/partials/ppa_not_signed_details.html'
    success_url = ""
    object = None

    def post(self, request, *args, **kwargs):
        ppa_not_signed = get_ppa_not_signed_by_user(self.request.user)
        context = self.get_context_data(**kwargs)
        context["ppa_not_signed"] = ppa_not_signed
        ppa = ppa_not_signed[0] if ppa_not_signed else None
        url = ""
        if ppa:
            url = reverse_lazy('contract_edit', kwargs={'building_id': str(ppa.supply_point.building.id),
                                                        'meter_id': str(ppa.supply_point.id),
                                                        'contract_id': str(ppa.id)})
        return JsonResponse({"url": url}, safe=False)


class MeterConsumption(TemplateView):

    def get(self, request, *args, **kwargs):
        message = 'Consumption is not registered'
        month_consumption = 0
        ok = False
        meter = Meter.objects.get(id=kwargs["meter_id"])
        contract = ContractBase.objects.get(supply_point=meter.id)
        if isinstance(contract, PPAContract):
            current_date = datetime.now()
            if contract.should_register_consumption:
                last_day_of_prev_month = date.today().replace(day=1) - timedelta(days=1)
                start_day_of_prev_month = date.today().replace(day=1) - timedelta(days=last_day_of_prev_month.day)
                consumption = get_data(meter, start_day_of_prev_month, last_day_of_prev_month)
                if not consumption.empty:
                    month_consumption = consumption.sum().values[0]
                contract.bc_last_registered_consumption_date = current_date.strftime("%m/%d/%Y")
                contract.save()
                message = 'Consumption registered'
                ok = True
            else:
                ok = False
                message = 'Consumption is already registered'
        return JsonResponse({"message": message, 'ok': ok, 'month_consumption': month_consumption})


class BlockchainAlert(TemplateView):

    def post(self, request, *args, **kwargs):
        from core.alerts.models import AlertType
        from core.alerts.manager import AlertManager
        try:
            contract_id = request.POST.get('contract_id')
            contract: PPAContract = PPAContract.objects.get(id=contract_id)
            if request.POST.get('alert_type') == AlertType.PPA_MODIFIED_CONTRACT_BC:
                AlertManager().evaluate_alert(contract.building, AlertType.PPA_MODIFIED_CONTRACT_BC, contract, **kwargs)
            return JsonResponse({'success': True, 'message': 'ok'})
        except Exception as e:
            return JsonResponse({'success': False, 'message': str(e)})


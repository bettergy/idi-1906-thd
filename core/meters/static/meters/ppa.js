if (!library)
   var library = {};

library.json = {
   replacer: function(match, pIndent, pKey, pVal, pEnd) {
      var key = '<span class=json-key>';
      var val = '<span class=json-value>';
      var str = '<span class=json-string>';
      var r = pIndent || '';
      if (pKey)
         r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
      if (pVal)
         r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
      return r + (pEnd || '');
      },
   prettyPrint: function(obj) {
      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
      return JSON.stringify(obj, null, 3)
         .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
         .replace(/</g, '&lt;').replace(/>/g, '&gt;')
         .replace(jsonLine, library.json.replacer);
      }
   };

class PPA {

  web3Provider = null
  contract = null
  account = {}

  static async init() {
    return await this.initWeb3();
  }

  static async initWeb3() {

    // Modern dapp browsers...
    if (window.ethereum) {
      this.web3Provider = window.ethereum;
      try {
        // Request account access
        //await window.ethereum.enable();
        ethereum.autoRefreshOnNetworkChange = false;

        var accounts = await ethereum.request({ method: 'eth_requestAccounts' });
        if (accounts != undefined && accounts.length > 0) {
          this.account = accounts[0];
          console.log('Current account ' + JSON.stringify(this.account));
        }

      } catch (error) {
        // User denied account access...
        console.error("User denied account access");
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      this.web3Provider = window.web3.currentProvider;
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      // TODO 0 this.web3Provider = new Web3.providers.WebsocketProvider('wss://localhost:7545/ws');
    }

    web3 = new Web3(this.web3Provider);

    return this.initContract();
  }

  static async initContract() {
    console.info('initializing contract');

    var contractAbi = [
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "start",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "end",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "uint32",
            "name": "consumed",
            "type": "uint32"
          },
          {
            "indexed": false,
            "internalType": "uint32",
            "name": "generated",
            "type": "uint32"
          },
          {
            "indexed": false,
            "internalType": "uint32",
            "name": "cost",
            "type": "uint32"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "closedBy",
            "type": "address"
          }
        ],
        "name": "BalancePeriodClosed",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "string",
            "name": "clause",
            "type": "string"
          },
          {
            "indexed": false,
            "internalType": "uint32",
            "name": "delta",
            "type": "uint32"
          },
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "periodId",
            "type": "bytes32"
          }
        ],
        "name": "ClauseViolationAlert",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "string",
            "name": "meterId",
            "type": "string"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "meterAccount",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "uint32",
            "name": "energyAmount",
            "type": "uint32"
          },
          {
            "indexed": false,
            "internalType": "uint8",
            "name": "energyType",
            "type": "uint8"
          }
        ],
        "name": "EnergySaved",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "string",
            "name": "meterId",
            "type": "string"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "meterAccount",
            "type": "address"
          }
        ],
        "name": "MeterSet",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "investor",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "user",
            "type": "address"
          }
        ],
        "name": "PPACreated",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "user",
            "type": "address"
          }
        ],
        "name": "PPASigned",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "Paused",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "PauserAdded",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "PauserRemoved",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "uint8",
            "name": "counter",
            "type": "uint8"
          }
        ],
        "name": "SampleEvent",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "Unpaused",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "start",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "end",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "uint8",
            "name": "alertsCount",
            "type": "uint8"
          },
          {
            "indexed": false,
            "internalType": "bytes32",
            "name": "periodId",
            "type": "bytes32"
          },
          {
            "indexed": false,
            "internalType": "address",
            "name": "closedBy",
            "type": "address"
          }
        ],
        "name": "ValidationPeriodClosed",
        "type": "event"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "addPauser",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "isOwner",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "account",
            "type": "address"
          }
        ],
        "name": "isPauser",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "pause",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "paused",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "renouncePauser",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "unpause",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getMyPPAs",
        "outputs": [
          {
            "internalType": "bytes32[]",
            "name": "",
            "type": "bytes32[]"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "user",
            "type": "address"
          },
          {
            "internalType": "uint32",
            "name": "genAnualMin",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "genAnualMax",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "genPriceUnder",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "genPriceNormal",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "genPriceOver",
            "type": "uint32"
          },
          {
            "internalType": "string",
            "name": "otherFields",
            "type": "string"
          }
        ],
        "name": "createPPA",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          }
        ],
        "name": "getPPADetails",
        "outputs": [
          {
            "internalType": "address[2]",
            "name": "owners",
            "type": "address[2]"
          },
          {
            "internalType": "uint32[2]",
            "name": "generationRange",
            "type": "uint32[2]"
          },
          {
            "internalType": "uint32[3]",
            "name": "prices",
            "type": "uint32[3]"
          },
          {
            "internalType": "address[2]",
            "name": "metersAccounts",
            "type": "address[2]"
          },
          {
            "internalType": "string",
            "name": "consumMeterId",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "genMeterId",
            "type": "string"
          },
          {
            "internalType": "uint256",
            "name": "signed",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          }
        ],
        "name": "getPPATotals",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "absoluteStart",
            "type": "uint256"
          },
          {
            "internalType": "uint32",
            "name": "absoluteGenerated",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "absoluteConsumed",
            "type": "uint32"
          },
          {
            "internalType": "uint256",
            "name": "balanceStart",
            "type": "uint256"
          },
          {
            "internalType": "uint32",
            "name": "balanceGenerated",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "balanceConsumed",
            "type": "uint32"
          },
          {
            "internalType": "uint256",
            "name": "validationStart",
            "type": "uint256"
          },
          {
            "internalType": "uint32",
            "name": "validationGenerated",
            "type": "uint32"
          },
          {
            "internalType": "uint32",
            "name": "validationConsumed",
            "type": "uint32"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          },
          {
            "internalType": "string",
            "name": "meterId",
            "type": "string"
          },
          {
            "internalType": "uint8",
            "name": "meterType",
            "type": "uint8"
          },
          {
            "internalType": "address",
            "name": "meterAccount",
            "type": "address"
          }
        ],
        "name": "linkMeterToPPA",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          }
        ],
        "name": "signPPA",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "string",
            "name": "meterId",
            "type": "string"
          },
          {
            "internalType": "uint32",
            "name": "energyAmount",
            "type": "uint32"
          },
          {
            "internalType": "uint8",
            "name": "energyType",
            "type": "uint8"
          }
        ],
        "name": "saveEnergy",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          }
        ],
        "name": "closeBalancePeriod",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "bytes32",
            "name": "ppaId",
            "type": "bytes32"
          }
        ],
        "name": "closeValidationPeriod",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "sampleCall",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "payable": false,
        "stateMutability": "pure",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "sampleTransaction",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ];
    var contractAddress = "0xA86a55f8357E691099BCa785d89D57a2d1CCbA84"

    // Get the necessary contract artifact file and instantiate it with @truffle/contract
    PPA.contract = new web3.eth.Contract(contractAbi, contractAddress)

    // Set the provider for our contract
    PPA.contract.setProvider(PPA.web3Provider);

    console.info('contract initialized');

  }

  static async getMyPPAs() {
    try {
      const val = await PPA.contract.methods.getMyPPAs().call({
        "from": PPA.account
      }, (err, txHash) => {
        console.log(err); console.log(txHash);
      });
      return val;

    } catch (error) {
      console.log(error)
    }

  }

  static async createPPA(user, genAnnualMin, genAnnualMax, genPriceUnder, genPriceNormal, genPriceOver, otherFields) {
    var options = { 'from': PPA.account, gas: 2000000 }

    return PPA.contract.methods.createPPA(user, genAnnualMin, genAnnualMax, genPriceUnder, genPriceNormal, genPriceOver, otherFields)
      .send(options);
  }

  static async getPPADetails(ppaID) {
    try {
      const val = await PPA.contract.methods.getPPADetails(ppaID).call({
        "from": PPA.account
      }, (err, txHash) => {
        console.log(err);
        console.log(txHash);
      });

      return val;

    } catch (error) {
      console.log(error)
    }
  }

  static async getPPATotals(ppaID) {
    try {
      const totals = PPA.contract.methods.getPPATotals(ppaID).call({
        "from": PPA.account
      }, (err, txHash) => {
        console.log(err); console.log(txHash)
      });
      return totals;

    } catch (error) {
      console.log(error)
    }

  }

  static async getAccount(){
    try {
      var metamaskAddress = await web3.eth.getAccounts();
      return metamaskAddress[0]
    } catch (error) {
      throw "Error: getting account"
    }

  }

  static async signPPA(ppaID) {
    var options = { 'from': PPA.account, gas: 2000000 }

    return PPA.contract.methods.signPPA(ppaID).send(options);
  }

  static async linkMeterToPPA(ppaId, meterId, meterType, meterAccount) {
    let options = {'from': PPA.account, gas: 2000000}
    return PPA.contract.methods.linkMeterToPPA(ppaId, meterId, meterType, meterAccount).send(options);
  }

  static async saveEnergy(meterId, energyAmount, energyType) {
    var options = { 'from': PPA.account, gas: 2000000 }

    return PPA.contract.methods.saveEnergy(meterId, energyAmount, energyType).send(options)
  }

  static async subscribe(eventName, callback) {

    const eventJsonInterface = web3.utils._.find(contract._jsonInterface, o => o.name === eventName && o.type === 'event',);
    const options = {
        address: contract.options.address,
        topics: [eventJsonInterface.signature],
        fromBlock,
        toBlock
    };
    web3.eth.subscribe('logs', options, (error, result) => {
        if (!error) {
            const eventObj = web3.eth.abi.decodeLog(eventJsonInterface.inputs, result.data, result.topics.slice(1));
            callback(eventObj);
        }
    });

  }

  static async subscribe3(event, callback) {
    var contract = PPA.contract;



    contract.once('EnergySaved'
      , function (error, event) {
        console.log(event);
        callback(error, event);
      });

    contract.getPastEvents(event,
      function (error, events) { console.log(events); })
      .then(function (events) {
        console.log(`Event: ${events}`);
      });
  }


  static async subscribe2(eventName, callback, fromBlock, toBlock) {
    var contract = PPA.contract;
    const eventJsonInterface = web3.utils._.find(contract._jsonInterface, o => o.name === eventName && o.type === 'event',);
    const options = {
      address: contract.options.address,
      topics: [eventJsonInterface.signature],
    };
    web3.eth.subscribe('logs', options, (error, result) => {
      console.log(`log received: ${error}, RESULT: ${result}`);
      if (!error) {
        const eventObj = web3.eth.abi.decodeLog(eventJsonInterface.inputs, result.data, result.topics.slice(1));
        callback(eventObj);
      }
    });
  }

}

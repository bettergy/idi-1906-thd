


def get_ppa_not_signed_by_user(user):
    try:
        buildings = get_buildings(user).values_list('id')
        result = PPAContract.objects.filter(building__in=buildings, bc_signed=False)
    except:
        result = PPAContract.objects.none()
    return result